package com.example.contactlist;

import com.example.contactlist.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class ContactController {

    private final ContactService contactService;

    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("contacts", contactService.findAll());
        return "index";
    }

    @GetMapping("/contact/create")
    public String showCreateContactForm(Model model){
        model.addAttribute("contact", new Contact());
        model.addAttribute("isNew", true);
        return "formContact";
    }

    @GetMapping("/contact/edit/{id}")
    public String showEditForm(@PathVariable Long id, Model model){
        Contact contact = contactService.findById(id);
        if (contact != null){
            model.addAttribute("contact", contact);
            model.addAttribute("isNew", false);
            return "formContact";
        }
        return "redirect:/";
    }

    @PostMapping("/contact/save")
    public String saveContact(@ModelAttribute Contact contact){
        if (contact.getId()!=null){
            contactService.update(contact);
        }else {
            contactService.save(contact);
        }
        return "redirect:/";
    }

    @GetMapping("/contact/delete/{id}")
    public String deleteContact(@PathVariable Long id){
        contactService.deleteById(id);
        return "redirect:/";
    }

}
